import time
import json
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC 
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager

# VARIABLES
print('Initializing variables')
chromePath = 'C:/Users/polin/Downloads/chromedriver_win32/chromedriver.exe'
auth = {
  'url':'https://dream-velocity-3716.lightning.force.com/', 
  'user':'test-mzknczdaww3s@example.com ',
  'psw':'khf)wwg0gGffk'
}
countriesAndStates = {
  'AR' : {'09' : 'Salta', '10' : 'San Juan', '11' : 'San Luis', '12' : 'Santa Fé', '13' : 'Santiago del Estero', '14' : 'Tucumán', '16' : 'Chaco', '17' : 'Chubut', '18' : 'Formosa', '19' : 'Misiones', '20' : 'Neuquen', '21' : 'La Pampa', '22' : 'Rio Negro', '23' : 'Santa Cruz', '24' : 'Tierra de Fuego'},
  'AT' : {'B' : 'Burgenland', 'K' : 'Kärnten', 'S' : 'Salzburg', 'ST' : 'Steiermark', 'T' : 'Tirol', 'V' : 'Vorarlberg', 'W' : 'Wien'},
  'CN' : {'010' : 'Beijing', '020' : 'Shanghai', '030' : 'Tianjin', '040' : 'Nei Mongol', '050' : 'Shanxi', '060' : 'Hebei', '070' : 'Liaoning', '080' : 'Jilin', '090' : 'Heilongjiang', '100' : 'Jiangsu', '110' : 'Anhui', '120' : 'Shandong', '130' : 'Zhejiang', '140' : 'Jiangxi', '150' : 'Fujian', '160' : 'Hunan', '170' : 'Hubei', '180' : 'Henan', '190' : 'Guangdong', '200' : 'Hainan', '210' : 'Guangxi', '220' : 'Guizhou', '230' : 'Sichuan', '240' : 'Yunnan', '250' : 'Shaanxi', '260' : 'Gansu', '270' : 'Ningxia', '280' : 'Qinghai', '290' : 'Xinjiang', '300' : 'Xizang', '310' : 'Taiwa'},
  'DE' : {'01' : 'Schleswig-Holstein', '02' : 'Hamburg', '03' : 'Niedersachsen', '04' : 'Bremen', '05' : 'Nordrhein-Westfalen', '06' : 'Hessen', '07' : 'Rheinland-Pfalz', '08' : 'Baden-Württemberg', '09' : 'Bayern', '10' : 'Saarland', '11' : 'Berlin', '12' : 'Brandenburg', '13' : 'Mecklenburg-Vorpomm.', '14' : 'Sachsen', '15' : 'Sachsen-Anhalt', '16' : 'Thüringe'},
  'DK' : {'001' : 'Staden København', '002' : 'Århus amt', '003' : 'Bornholm amt', '004' : 'Frederiksborg amt', '005' : 'Fyn amt', '006' : 'København amt', '007' : 'Nordjylland amt', '008' : 'Ribe amt', '009' : 'Ringkøbing amt', '010' : 'Roskilde amt', '011' : 'Sønderjylland amt', '012' : 'Storstrøm amt', '013' : 'Vejle amt', '014' : 'Vestsjælland amt', '015' : 'Viborg amt', '016' : 'Grønlands am'},
  'ES' : {'01' : 'Alava', '02' : 'Albacete', '03' : 'Alicante', '04' : 'Almería', '05' : 'Avila', '06' : 'Badajoz', '07' : 'Baleares', '08' : 'Barcelona', '09' : 'Burgos', '10' : 'Cáceres', '11' : 'Cadiz', '12' : 'Castellón', '13' : 'Ciudad Real', '14' : 'Córdoba', '15' : 'La Coruña', '16' : 'Cuenca', '17' : 'Gerona', '18' : 'Granada', '19' : 'Guadalajara', '20' : 'Guipúzcoa', '21' : 'Huelva', '22' : 'Huesca', '23' : 'Jaén', '24' : 'León', '25' : 'Lérida', '26' : 'La Rioja', '27' : 'Lugo', '28' : 'Madrid', '29' : 'Málaga', '30' : 'Murcia', '31' : 'Navarra', '32' : 'Orense', '33' : 'Asturias', '34' : 'Palencia', '35' : 'Las Palmas', '36' : 'Pontevedra', '37' : 'Salamanca', '38' : 'Sta. Cruz Tenerife', '39' : 'Cantabria', '40' : 'Segovia', '41' : 'Sevilla', '42' : 'Soria', '43' : 'Tarragona', '44' : 'Teruel', '45' : 'Toledo', '46' : 'Valencia', '47' : 'Valladolid', '48' : 'Vizcaya', '49' : 'Zamora', '50' : 'Zaragoza', '001' : 'Ahvenanma'},
  'FI' : {'002' : 'Etelä-Suomi', '003' : 'Itä-Suomi', '004' : 'Lappi', '005' : 'Länsi-Suomi', '006' : 'Oul'},
  'FR' : {'01' : 'Ain', '02' : 'Aisne', '03' : 'Allier', '04' : 'Alpes Hte-Provence', '05' : 'Alpes Hautes', '06' : 'Alpes-Maritimes', '07' : 'Ardèche', '08' : 'Ardennes', '09' : 'Ariège', '10' : 'Aube', '11' : 'Aude', '12' : 'Aveyron', '13' : 'Bouches-du-Rhône', '14' : 'Calvados', '15' : 'Cantal', '16' : 'Charente', '17' : 'Charente-Maritime', '18' : 'Cher', '19' : 'Corrèze', '21' : 'Côte-d\'Or', '22' : 'Côtes-d\'Armor', '23' : 'Creuse', '24' : 'Dordogne', '25' : 'Doubs', '26' : 'Drôme', '27' : 'Eure', '28' : 'Eure-et-Loir', '29' : 'Finistère', '2A' : 'Corse-du-Sud', '2B' : 'Corse-du-Nord', '30' : 'Gard', '31' : 'Garonne Haute', '32' : 'Gers', '33' : 'Gironde', '34' : 'Hérault', '35' : 'Ille-et-Vilaine', '36' : 'Indre', '37' : 'Indre-et-Loire', '38' : 'Isère', '39' : 'Jura', '40' : 'Landes', '41' : 'Loir-et-Cher', '42' : 'Loire', '43' : 'Loire Haute', '44' : 'Loire-Atlantique', '45' : 'Loiret', '46' : 'Lot', '47' : 'Lot-et-Garonne', '48' : 'Lozère', '49' : 'Maine-et-Loire', '50' : 'Manche', '51' : 'Marne', '52' : 'Marne Haute', '53' : 'Mayenne', '54' : 'Meurthe-et-Moselle', '55' : 'Meuse', '56' : 'Morbihan', '57' : 'Moselle', '58' : 'Nièvre', '59' : 'Nord', '60' : 'Oise', '61' : 'Orne', '62' : 'Pas-de-Calais', '63' : 'Puy-de-Dôme', '64' : 'Pyrénées-Atlantiques', '65' : 'Pyrénées Hautes', '66' : 'Pyrénées-Orientales', '67' : 'Rhin Bas', '68' : 'Rhin Haut', '69' : 'Rhône', '70' : 'Saône Haute', '71' : 'Saône-et-Loire', '72' : 'Sarthe', '73' : 'Savoie', '74' : 'Savoie Haute', '75' : 'Paris', '76' : 'Seine-Maritime', '77' : 'Seine-et-Marne', '78' : 'Yvelines', '79' : 'Sèvres Deux', '80' : 'Somme', '81' : 'Tarn', '82' : 'Tarn-et-Garonne', '83' : 'Var', '84' : 'Vaucluse', '85' : 'Vendée', '86' : 'Vienne', '87' : 'Vienne Haute', '88' : 'Vosges', '89' : 'Yonne', '90' : 'Territ.-de-Belfort', '91' : 'Essonne', '92' : 'Hauts-de-Seine', '93' : 'Seine-Saint-Denis', '94' : 'Val-de-Marne', '95' : 'Val-d\'Oise', '97' : 'D.O.M.-T.O.M.', '971' : 'Guadeloupe', '972' : 'Martinique', '973' : 'Guyane', '974' : 'Réunion', '975' : 'Saint-Pierre-et-Miq.', '976' : 'Wallis-et-Futuna', '99' : 'Hors-Franc'},
  'GR' : {'01' : 'Aitolia kai Akarnan.', '02' : 'Akhaia', '03' : 'Argolis', '04' : 'Arkadhia', '05' : 'Arta', '06' : 'Attiki', '07' : 'Dhodhekanisos', '08' : 'Dhrama', '09' : 'Evritania', '10' : 'Evros', '11' : 'Evvoia', '12' : 'Florina', '13' : 'Fokis', '14' : 'Fthiotis', '15' : 'Grevena', '16' : 'Ilia', '17' : 'Imathia', '18' : 'Ioannina', '19' : 'Iraklion', '20' : 'Kardhitsa', '21' : 'Kastoria', '22' : 'Kavala', '23' : 'Kefallinia', '24' : 'Kerkira', '25' : 'Khalkidhiki', '26' : 'Khania', '27' : 'Khios', '28' : 'Kikladhes', '29' : 'Kilkis', '30' : 'Korinthia', '31' : 'Kozani', '32' : 'Lakonia', '33' : 'Larisa', '34' : 'Lasithi', '35' : 'Lesvos', '36' : 'Levkas', '37' : 'Magnisia', '38' : 'Messinia', '39' : 'Pella', '40' : 'Pieria', '41' : 'Piraievs', '42' : 'Preveza', '43' : 'Rethimni', '44' : 'Rodhopi', '45' : 'Samos', '46' : 'Serrai', '47' : 'Thesprotia', '48' : 'Thessaloniki', '49' : 'Trikala', '50' : 'Voiotia', '51' : 'Xanthi', '52' : 'Zakintho'},
  'HR' : {'01' : 'Bjelovar-Bilogora', '02' : 'Stadt Zagreb', '03' : 'Dubrovnik-Neretva', '04' : 'Istra', '05' : 'Karlovac', '06' : 'Koprivnica-Krizevci', '07' : 'Lika-Senj', '08' : 'Medimurje', '09' : 'Osijek-Baranja', '10' : 'Pozega-Slavonija', '11' : 'Primorje-Gorski Kot.', '12' : 'Sibenik', '13' : 'Sisak-Moslavina', '14' : 'Slavonski', '15' : 'Brod-Posavina', '16' : 'Split-Dalmatia', '17' : 'Varazdin', '18' : 'Virovitica-Podravina', '19' : 'Vukovar-Srijem', '20' : 'Zadar-Knin', '21' : 'Zagre'},
  'HU' : {'01' : 'Bacs-Kiskun', '02' : 'Baranya', '03' : 'Bekes', '04' : 'Bekescsaba', '05' : 'Borsod-Abauj-Zemplen', '06' : 'Budapest', '07' : 'Csongrad', '08' : 'Debrecen', '09' : 'Dunaujvaros', '10' : 'Eger', '11' : 'Fejer', '12' : 'Gyor', '13' : 'Gyor-Moson-Sopron', '14' : 'Hajdu-Bihar', '15' : 'Heves', '16' : 'Hodmezovasarhely', '17' : 'Jasz-Nagykun-Szolnok', '18' : 'Kaposvar', '19' : 'Kecskemet', '20' : 'Komarom-Esztergom', '21' : 'Miskolc', '22' : 'Nagykanizsa', '23' : 'Nograd', '24' : 'Nyiregyhaza', '25' : 'Pecs', '26' : 'Pest', '27' : 'Somogy', '28' : 'Sopron', '29' : 'Szabolcs-Szat.-Bereg', '30' : 'Szeged', '31' : 'Szekesfehervar', '32' : 'Szolnok', '33' : 'Szombathely', '34' : 'Tatabanya', '35' : 'Tolna', '36' : 'Vas', '37' : 'Veszprem', '38' : 'Zala', '39' : 'Zalaegersze'},
  'ID' : {'01' : 'DKI Jakarta Jakarta', '02' : 'Jawa Barat West Java', '03' : 'Jawa Tengah Central', '04' : 'Jawa Timur East Java', '05' : 'DI Yogyakarta Yogyak', '06' : 'DI Aceh Aceh', '07' : 'Sumatera Utara North', '08' : 'Sumatera Barat West', '09' : 'Riau Riau', '10' : 'Jambi Jambi', '11' : 'Sumatera Selatan Sou', '12' : 'Bengkulu Bengkulu', '13' : 'Lampung Lampung', '14' : 'Kalimantan Selatan S', '15' : 'Kalimantan Barat Wes', '16' : 'Kalimantan Tengah Ce', '17' : 'Kalimantan Timur Eas', '18' : 'Sulawesi Selatan Sou', '19' : 'Sulawesi Tenggara So', '20' : 'Sulawesi Tengah Cent', '21' : 'Sulawesi Utara North', '22' : 'Bali Bali', '23' : 'Nusa Tenggara Barat', '24' : 'Nusa Tenggara Timur', '25' : 'Maluku Maluku', '26' : 'Irian Jaya Irian Jay', '27' : 'Timor Timur East Ti'},
  'IE' : {'CK' : 'Cork', 'CL' : 'Clare', 'CV' : 'Cavan', 'DB' : 'Dublin', 'DG' : 'Donegal', 'GW' : 'Galway', 'KD' : 'Kildare', 'LF' : 'Longford', 'LI' : 'Limerick', 'LT' : 'Louth', 'MH' : 'Monaghan', 'MT' : 'Meath', 'MY' : 'Mayo', 'OF' : 'Offaly', 'RC' : 'Roscommon', 'SG' : 'Sligo', 'TP' : 'Tipperary', 'WF' : 'Waterford', 'WK' : 'Wicklow', 'WM' : 'Westmeat'}
}

# WEB INTERACTION

# login to Salesforce
print('Opening the browser')
driver = webdriver.Chrome(chromePath)
driver.get(auth['url'])
driver.find_element(By.ID, 'username').send_keys(auth['user'])
driver.find_element(By.ID, 'password').send_keys(auth['psw'])
driver.find_element(By.ID, 'Login').click()
WebDriverWait(driver, 30).until(EC.url_contains('home'))

# add States
try:
    for countryCode in countriesAndStates.keys():

        print('\n__________ Country:: ' + countryCode + ' __________')
        driver.get(auth['url'] + 'i18n/ConfigureCountry.apexp?countryIso=' + countryCode + '&setupid=AddressCleanerOverview')
        
        for stateCode in countriesAndStates[countryCode].keys():
                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'configurecountry:form:stateRelatedListComponent:configStateCountryRelList:list:j_id50:buttonAddNew')))

                # open new state page, polulate fields
                driver.find_element(By.ID, 'configurecountry:form:stateRelatedListComponent:configStateCountryRelList:list:j_id50:buttonAddNew').click()
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id9:nameSectionItem:editName').click()
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id9:nameSectionItem:editName').send_keys(countriesAndStates[countryCode][stateCode])
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id9:codeSectionItem:editIsoCode').click()
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id9:codeSectionItem:editIsoCode').send_keys(stateCode)
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id9:activeSectionItem:editActive').click()
                WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.ID, 'configurenew:j_id1:blockNew:j_id9:visibleSectionItem:editVisible')))
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id9:visibleSectionItem:editVisible').click()

                # save, wait for results
                driver.find_element(By.ID, 'configurenew:j_id1:blockNew:j_id43:addButton').click()

                WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.CSS_SELECTOR, '.msgIcon')))
                msgTitle = driver.find_element(By.CSS_SELECTOR, '.msgIcon').get_attribute('title')

                # if all went successful - we're redirected to country page, and see green success icon
                if msgTitle == 'CONFIRM':
                    print('Added State: ' + stateCode + ' - ' + countriesAndStates[countryCode][stateCode])

                # in case of error - we stay on new state page and pring out the error messages from the page
                else:
                    print('ERROR in adding ' + stateCode + ' - ' + countriesAndStates[countryCode][stateCode])
                    errorMsgElement = driver.find_element(By.CSS_SELECTOR, '.messageTable')
                    if errorMsgElement.find_elements_by_tag_name('li'):
                        for errorMsg in errorMsgElement.find_elements_by_tag_name('li'):
                            print('  - ' + errorMsg.text.replace('\n', ' '))
                    else:
                        print('  - ' + errorMsgElement.text.replace('\n', ' '))
                    driver.get(auth['url'] + 'i18n/ConfigureCountry.apexp?countryIso=' + countryCode + '&setupid=AddressCleanerOverview')
except:
    driver.quit()
